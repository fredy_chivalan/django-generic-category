from django.test import TestCase
from django_src.apps.category.models import Category


class CategoryTest(TestCase):
    """docstring for ."""

    def setUp(self):
        self.category = Category.objects.create(
            title='category title',
            slug='category-title',
        )

    def test_category_creation(self):
        self.assertEqual(self.category.title, self.category.__str__())
