import uuid
from django.conf import settings
from django.db import models
from django.utils.translation import gettext_lazy as _
from django.db.models.signals import pre_save
from django.utils.text import slugify


class Category(models.Model):
    """Category model to be used for categorization of content.

    Categories are high level constructs to be used for grouping and organizing
    content, thus creating a site"s table of contents.
    """
    id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    title = models.CharField(_('Category title'), max_length=100, unique=True)
    slug = models.SlugField(max_length=100, db_index=True, blank=True)
    image = models.ImageField(
        upload_to=settings.UPLOAD_TO,
        blank=settings.BLANK_IMAGE,
        null=settings.NULL_IMAGE
    )
    description = models.TextField(
        _('Category description'),
        null=True,
        blank=True
    )
    created = models.DateTimeField(
        auto_now_add=True,
        verbose_name=_('Creation date'),
        help_text=_('Date time on which the object was created.'),
    )
    modified = models.DateTimeField(
        auto_now=True,
        verbose_name=_('Modified date'),
        help_text=_('Date time on which the object was last modified.')
    )

    def __str__(self):
        return self.title

    class Meta:
        ordering = ['title']
        verbose_name = "Category"
        verbose_name_plural = "Categories"


def create_slug(instance, new_slug=None):
    slug = slugify(instance.title)
    if new_slug is not None:
        slug = new_slug
    qs = Category.objects.filter(slug=slug).order_by("-title")
    exists = qs.exists()
    if exists:
        new_slug = "%s-%s" %(slug, qs.first().title)  # noqa
        return create_slug(instance, new_slug=new_slug)
    return slug


def pre_save_post_receiver(sender, instance, *args, **kwargs):
    if not instance.slug:
        instance.slug = create_slug(instance)


pre_save.connect(pre_save_post_receiver, sender=Category)
