=====
Category
=====

Generic category model for django projects.

The categories are built to group and organize content (products, books, clothing, among others), being located at the top level of a site.

Detailed documentation is in the "docs" directory.

Requirements
------------
#. Python 3.8.x
#. Django 3.2.x

Quick start
-----------
1. Install or add ``django-generic-category``::

    pip install django-generic-category

2. Add "category" to your INSTALLED_APPS setting like this::

    INSTALLED_APPS = [
        ...
        'category',
        ...
    ]

3. Set in the ``settings.py`` file the desired folder name to save the category images, as well as the option of null and blank::

    ...
    UPLOAD_TO='Category'
    BLANK_IMAGE=True
    NULL_IMAGE=True


4. Run ``python manage.py migrate`` to create the category models.

5. Start the development server and visit http://127.0.0.1:8000/admin/
   to create a category (you'll need the Admin app enabled).


Usage
-----

Enable categorization and/or tagging on a model by creating ``ForeignKey`` fields to the models provided by ``django-generic-category``, for example::

    from django import models

    class MyModel(models.Model):
        categories = models.ForeignKey(
            'category.Category',
            on_delete=models.CASCADE
        )


Models
------
The Category model contains the following attributes::

    class Category(models.Model):
        title = models.CharField()
        slug = models.SlugField()
        image = models.ImageField(
            upload_to=settings.UPLOAD_TO,
            blank=settings.BLANK_IMAGE,
            null=settings.NULL_IMAGE
        )
        description = models.TextField()
        created = models.DateTimeField()
        modified = models.DateTimeField()



